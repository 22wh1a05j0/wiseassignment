#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <windows.h>
#include <conio.h>


typedef struct {
    int x;
    int y;
} Point;


typedef enum {
    SHAPE_O,
    SHAPE_I,
    SHAPE_T,
    SHAPE_L,
    SHAPE_J,
    SHAPE_S,
    SHAPE_Z,
    NUM_SHAPES
} TetrisShape;


typedef struct {
    TetrisShape shape;
    Point blocks[4];  
} TetrisPiece;


typedef struct Node {
    TetrisPiece data;
    struct Node* next;
} Node;


typedef struct {
    Node* front;
    Node* rear;
} Queue;

 
void initializeQueue(Queue* q);
int isQueueEmpty(Queue* q);
void enqueue(Queue* q, TetrisPiece p);
TetrisPiece dequeue(Queue* q);
void printBoard(int board[10][20], TetrisPiece* currentPiece, int score);
void moveDown(TetrisPiece* currentPiece);
void moveLeft(TetrisPiece* currentPiece);
void moveRight(TetrisPiece* currentPiece);
void rotate(TetrisPiece* currentPiece);
void updateBoard(int board[10][20], TetrisPiece* currentPiece, int* score);
bool collisionDetected(int board[10][20], TetrisPiece* currentPiece);
void handleInput(TetrisPiece* currentPiece);
void hideCursor(void);
void showCursor(void);
void initializeTetrisPieces(Queue* pieceQueue);
void gotoXY(int x, int y);


void initializeQueue(Queue* q) {
    q->front = q->rear = NULL;
}


int isQueueEmpty(Queue* q) {
    return (q->front == NULL);
}


void enqueue(Queue* q, TetrisPiece p) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = p;
    newNode->next = NULL;

    if (q->rear == NULL) {
        q->front = q->rear = newNode;
        return;
    }

    q->rear->next = newNode;
    q->rear = newNode;
}


TetrisPiece dequeue(Queue* q) {
    if (isQueueEmpty(q)) {
        TetrisPiece p;
        p.shape = -1; 
        return p;
    }

    Node* temp = q->front;
    TetrisPiece p = temp->data;

    q->front = q->front->next;

    if (q->front == NULL) {
        q->rear = NULL;
    }

    free(temp);
    return p;
}

void printBoard(int board[10][20], TetrisPiece* currentPiece, int score) {
    int i, j, k;
    gotoXY(0, 0); 
    for (i = 0; i < 20; i++) { 
        for (j = 0; j < 10; j++) { 
            int blockIndex = -1;
            for (k = 0; k < 4; k++) {
                if (currentPiece->blocks[k].x == j && currentPiece->blocks[k].y == i) { 
                    blockIndex = k;
                    break;
                }
            }

            if (blockIndex != -1) {
                printf("X ");
            } else if (board[j][i] == 1) { 
                printf("X ");
            } else {
                printf(". ");
            }
        }
        printf("\n");
    }
    printf("Score: %d\n", score);
}


void gotoXY(int x, int y) {
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}


void moveDown(TetrisPiece* currentPiece) {
    int i;
    for (i = 0; i < 4; i++) {
        currentPiece->blocks[i].y++; 
    }
}


void moveLeft(TetrisPiece* currentPiece) {
    int i;
    for (i = 0; i < 4; i++) {
        currentPiece->blocks[i].x--; 
    }
}


void moveRight(TetrisPiece* currentPiece) {
    int i;
    for (i = 0; i < 4; i++) {
        currentPiece->blocks[i].x++; 
    }
}


void rotate(TetrisPiece* currentPiece) {

    Point originalBlocks[4];
    int i;
    for (i = 0; i < 4; i++) {
        originalBlocks[i] = currentPiece->blocks[i];
    }


    Point rotationCenter = currentPiece->blocks[0];

    for (i = 0; i < 4; i++) {
        int relativeX = originalBlocks[i].x - rotationCenter.x;
        int relativeY = originalBlocks[i].y - rotationCenter.y;

        int rotatedX = -relativeY;
        int rotatedY = relativeX;

        currentPiece->blocks[i].x = rotationCenter.x + rotatedX;
        currentPiece->blocks[i].y = rotationCenter.y + rotatedY;
    }
}

void updateBoard(int board[10][20], TetrisPiece* currentPiece, int* score) {
    int i;
    for (i = 0; i < 4; i++) {
        board[currentPiece->blocks[i].x][currentPiece->blocks[i].y] = 1;  
    }

    *score += 100;

    int row, col;
    
    for (row = 0; row < 20; row++) {
        int count = 0;
        for (col = 0; col < 10; col++) {
            if (board[col][row] == 1) {
                count++;
            }
        }
        if (count == 10) {
            int r, c;
           
            for (r = row; r > 0; r--) {
                for (c = 0; c < 10; c++) {
                    board[c][r] = board[c][r - 1];
                }
            }

            for (c = 0; c < 10; c++) {
                board[c][0] = 0;
            }
            *score += 500; 
        }
    }
}


bool collisionDetected(int board[10][20], TetrisPiece* currentPiece) {
    int i;
    for (i = 0; i < 4; i++) {
        if (board[currentPiece->blocks[i].x][currentPiece->blocks[i].y] == 1) {
            return true;  
        }
    }
    return false;  
}

void handleInput(TetrisPiece* currentPiece) {
    if (_kbhit()) {
        char key = _getch();
        switch (key) {
            case 'a':
                moveLeft(currentPiece);
                break;
            case 'd':
                moveRight(currentPiece);
                break;
            case 's':
                moveDown(currentPiece);
                break;
            case 'w':
                rotate(currentPiece);
                break;
            default:
                break;
        }
    }
}


void hideCursor(void) {
    CONSOLE_CURSOR_INFO cursorInfo;
    cursorInfo.dwSize = 1;
    cursorInfo.bVisible = FALSE;
    SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursorInfo);
}

void showCursor(void) {
    CONSOLE_CURSOR_INFO cursorInfo;
    cursorInfo.dwSize = 1;
    cursorInfo.bVisible = TRUE;
    SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursorInfo);
}

void initializeTetrisPieces(Queue* pieceQueue) {
    TetrisPiece pieces[NUM_SHAPES] = {
        { SHAPE_O, {{0, 0}, {0, 1}, {1, 0}, {1, 1}} },
        { SHAPE_I, {{0, 0}, {0, 1}, {0, 2}, {0, 3}} },
        { SHAPE_T, {{0, 1}, {1, 0}, {1, 1}, {1, 2}} },
        { SHAPE_L, {{0, 0}, {1, 0}, {1, 1}, {1, 2}} },
        { SHAPE_J, {{0, 2}, {1, 0}, {1, 1}, {1, 2}} },
        { SHAPE_S, {{0, 1}, {0, 2}, {1, 0}, {1, 1}} },
        { SHAPE_Z, {{0, 0}, {0, 1}, {1, 1}, {1, 2}} }
    };

    int i;
    for (i = 0; i < 4; i++) {
        int j;
        for (j = 0; j < NUM_SHAPES; j++) {
            enqueue(pieceQueue, pieces[j]);
        }
    }
}

int main(void) {
    int board[10][20] = {0}; 
    int score = 0; 

    Queue pieceQueue;
    initializeQueue(&pieceQueue);

    initializeTetrisPieces(&pieceQueue);

    TetrisPiece currentPiece = dequeue(&pieceQueue);  

    int iterations = 0;
    int maxIterations = 1000;  

    hideCursor(); 

    while (iterations < maxIterations) {
        printBoard(board, &currentPiece, score);

        printf("Press 'a' to move left, 'd' to move right, 's' to move down, 'w' to rotate.\n");

        handleInput(&currentPiece);

        
        if (collisionDetected(board, &currentPiece)) {
            printf("Game Over! Your Score: %d\n", score);
            break;
        }

        if (iterations % 10 == 0) {
            moveDown(&currentPiece);
        }

        if (currentPiece.blocks[0].y == 19 || currentPiece.blocks[1].y == 19 || currentPiece.blocks[2].y == 19 || currentPiece.blocks[3].y == 19 ||
            board[currentPiece.blocks[0].x][currentPiece.blocks[0].y + 1] == 1 ||
            board[currentPiece.blocks[1].x][currentPiece.blocks[1].y + 1] == 1 ||
            board[currentPiece.blocks[2].x][currentPiece.blocks[2].y + 1] == 1 ||
            board[currentPiece.blocks[3].x][currentPiece.blocks[3].y + 1] == 1) {
            updateBoard(board, &currentPiece, &score);  
            currentPiece = dequeue(&pieceQueue);  
        }

        Sleep(50); 

        iterations++;
    }

    showCursor(); 

    printf("Final Score: %d\n", score);

    return 0;
}

